package com.tybur.blog.dto;

import com.tybur.blog.models.Food;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlSchema;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Food")
public class FoodDto {

    @Schema(description = "Identification number, it doesn't required to be pointed out when add", example = "1")
    private Long id;
    @Schema(description = "Title of the product", example = "Potato chips")
    private String product;
    @Schema(description = "Currency of the country of origin of the goods", example = "Uzbek soums")
    private String currency;
    @Schema(description = "Manufacturer's brand", example = "Lay's")
    private String company;


    public static FoodDto from(Food food) {
        return FoodDto.builder().
                id(food.getId())
                .product(food.getProduct())
                .currency(food.getCurrency())
                .company(food.getCompany())
                .build();
    }

    public static List<FoodDto> from(List<Food> foodList) {
        return foodList.stream().map(FoodDto::from).collect(Collectors.toList());
    }
}
