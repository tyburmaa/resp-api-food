package com.tybur.blog.dto;

import lombok.Data;

@Data
public class BuyFoodDto {

    private Long foodId;
}
