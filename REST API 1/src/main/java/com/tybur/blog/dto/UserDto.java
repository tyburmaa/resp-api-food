package com.tybur.blog.dto;

import com.tybur.blog.models.User;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Данные о пользователе")
public class UserDto {
    @Schema(description = "Идентификатор")
    private Long id;
    @Schema(description = "Электронная почта", example = "123@gmail.com")
    private String email;

    @Schema(description = "Password", example = "QWERTY")
    private String password;

    public static UserDto from(User user) {
        return UserDto.builder()
                .id(user.getId())
                .email(user.getEmail())
                .password(user.getHashPassword())
                .build();
    }

    public static List<UserDto> from(List<User> users) {
        return users.stream()
                .map(UserDto::from)
                .collect(Collectors.toList());
    }

}

