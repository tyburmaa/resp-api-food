package com.tybur.blog.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@Schema(description = "Данные о пользователе")
public class SignUpDto {
    @Schema(description = "Электронная почта", example = "123@gmail.com")
    @NotBlank(message = "{dto.email.notBlank}")
    private String email;

    @Schema(description = "Password", example = "QWERTY")
    @NotBlank(message = "{dto.password.notBlank}")
    private String password;
}

