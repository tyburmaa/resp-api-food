package com.tybur.blog.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FoodPage {

    private List<FoodDto> foodDtoList;
    @Schema(description = "total amount of pages")
    private Integer totalPages;
    @Schema(description = "size of each page by number of its content")
    private Integer pageSize;

}
