package com.tybur.blog.dto.jsonplaceholder;

import lombok.Data;

@Data
public class PostDto {

    private Long id;
    private Long userId;
    private String title;
    private String body;

}
