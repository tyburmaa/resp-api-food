package com.tybur.blog.dto;

import com.tybur.blog.models.Buyer;
import com.tybur.blog.models.Food;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Buyer")
public class BuyerDto {
    @Schema(description = "Identification number, it doesn't required to be pointed out when add", example = "1")
    private Long id;
    @Schema(description = "first name of the person who is buyer", example = "Santa")
    private String firstName;
    @Schema(description = "last name of the person who is buyer", example = "Klaus")
    private String lastName;
    @Schema(description = "buyer's place of living", example = "Jamaica")
    private String domicile;

    private List<FoodDto> food;

    public static BuyerDto from (Buyer buyer) {
        BuyerDto result = BuyerDto.builder()
                .id(buyer.getId())
                .firstName(buyer.getFirstName())
                .lastName(buyer.getLastName())
                .domicile(buyer.getDomicile())
                .build();
        //преобразовываем сет в список, добавляем покупателю еду
        if (buyer.getFoodSet() != null) {
            result.setFood(FoodDto.from(new ArrayList<>(buyer.getFoodSet())));
        }
        return result;

    }
    public static List<BuyerDto> from(List<Buyer> buyerList) {
        return buyerList.stream().map(BuyerDto::from).collect(Collectors.toList());
    }
}
