package com.tybur.blog.services.impl;

import com.tybur.blog.dto.BuyFoodDto;
import com.tybur.blog.dto.BuyerDto;
import com.tybur.blog.exceptions.BuyerNotFoundException;
import com.tybur.blog.exceptions.FoodNotFoundException;
import com.tybur.blog.models.Buyer;
import com.tybur.blog.models.Food;
import com.tybur.blog.repositories.BuyerRepository;
import com.tybur.blog.repositories.FoodRepository;
import com.tybur.blog.services.BuyerService;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.tybur.blog.dto.BuyerDto.from;

@Service
@RequiredArgsConstructor
public class BuyerServiceImpl implements BuyerService {

    private BuyerRepository buyerRepository;
    private FoodRepository foodRepository;
    @Transactional
    @Override
    public BuyerDto addBuyer(BuyerDto buyerDto) {
        Buyer newBuyer = Buyer.builder()
                .firstName(buyerDto.getFirstName())
                .lastName(buyerDto.getLastName())
                .domicile(buyerDto.getDomicile())
                .build();
        buyerRepository.save(newBuyer);
        return from(newBuyer);
    }

    @Transactional
    @Override
    public BuyerDto buyFood(Long buyerId, BuyFoodDto buyFoodDto) {
        Buyer buyer = buyerRepository.findById(buyerId).orElseThrow(BuyerNotFoundException::new);
        Food food = foodRepository.findById(buyFoodDto.getFoodId()).orElseThrow(FoodNotFoundException::new);

        buyer.getFoodSet().add(food);
        buyerRepository.save(buyer);
        return from(buyer);

    }
}

