package com.tybur.blog.services;

import com.tybur.blog.dto.FoodDto;
import com.tybur.blog.dto.FoodPage;
import org.springframework.stereotype.Service;

import java.util.List;


public interface FoodService {

    FoodDto addFood(FoodDto foodDto);

    FoodPage getAll(int page);

    FoodDto getFood(Long foodId);

    FoodDto updateFood(Long newFood, FoodDto foodDto);

    void deleteFood(Long foodId);

    void reserveFood(Long foodId);
}
