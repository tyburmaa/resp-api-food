package com.tybur.blog.services.impl;

import com.tybur.blog.dto.PostsTextDto;
import com.tybur.blog.dto.jsonplaceholder.PostDto;
import com.tybur.blog.services.PostsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class PostsServiceImpl implements PostsService {
    private final String POSTS_API_URL = "/posts";
    private final RestTemplate restTemplate;

    @Value("${jsonplaceholder.base.url}")
    private String jsonPlaceholderBaseUrl;

    @Override
    public PostsTextDto getTexts() {
      ResponseEntity<PostDto[]> postsResponse = restTemplate.getForEntity(jsonPlaceholderBaseUrl + POSTS_API_URL,
              PostDto[].class);

      PostDto[] posts = postsResponse.getBody();


        List<String> texts = Arrays
                .stream(Objects.requireNonNull(posts))
                .map(PostDto::getBody)
                .toList();

        return PostsTextDto.builder()
                .texts(texts)
                .build();

    }
}
