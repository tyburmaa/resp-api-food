package com.tybur.blog.services.impl;

import com.tybur.blog.dto.SignUpDto;
import com.tybur.blog.dto.UserDto;
import com.tybur.blog.models.User;
import com.tybur.blog.repositories.UsersRepository;
import com.tybur.blog.services.SignUpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import javax.validation.Validator;

import java.util.Set;

import static com.tybur.blog.dto.UserDto.from;

@Slf4j
@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {

    private final UsersRepository usersRepository;
    private final Validator validator;

    @Override
    public UserDto signUp(SignUpDto signUpData) {
        User user = User.builder()
                .email(signUpData.getEmail())
                .hashPassword(signUpData.getPassword())
                .state(User.State.CONFIRMED)
                .role(User.Role.USER)
                .build();
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        //увидим список при возможных исключениях

        if (violations.isEmpty()) {
            usersRepository.save(user);
            return from(user);

        } else {
            throw  new ValidationException(processViolations(violations));
            //return UserDto.from(user);
        }
    }

    private String processViolations(Set<ConstraintViolation<User>> violations) {
        //validation on service layer
        StringBuilder message = new StringBuilder();

        message.append("Violations[");
        for (ConstraintViolation<User> violation : violations) {
            message.append("field(")
                    .append(violation.getPropertyPath())
                    .append(")")
                    .append(", message -  ")
                    .append(violation.getMessage())
                    .append(", ");
            //   log.error(violation.getPropertyPath() + "" + violation.getMessage());
        }

        return message.append("]").toString();
    }
}
