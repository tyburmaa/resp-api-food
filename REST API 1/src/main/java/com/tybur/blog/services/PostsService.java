package com.tybur.blog.services;

import com.tybur.blog.dto.PostsTextDto;

public interface PostsService {

    PostsTextDto getTexts();
}
