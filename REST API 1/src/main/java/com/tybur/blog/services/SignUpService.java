package com.tybur.blog.services;

import com.tybur.blog.dto.SignUpDto;
import com.tybur.blog.dto.UserDto;

public interface SignUpService {
    UserDto signUp(SignUpDto signUpData);
}
