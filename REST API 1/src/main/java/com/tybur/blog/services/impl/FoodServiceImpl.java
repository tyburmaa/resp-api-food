package com.tybur.blog.services.impl;

import com.tybur.blog.dto.FoodDto;
import com.tybur.blog.dto.FoodPage;
import com.tybur.blog.exceptions.FoodNotFoundException;
import com.tybur.blog.models.Food;
import com.tybur.blog.repositories.FoodRepository;
import com.tybur.blog.services.FoodService;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.tybur.blog.dto.FoodDto.from;


@Service
@RequiredArgsConstructor
@Builder
public class FoodServiceImpl implements FoodService {


    private final static int DEFAULT_PAGE_SIZE = 5;
    private final FoodRepository foodRepository;

    @Override
    public FoodDto addFood(FoodDto foodDto) {
        Food newFood = Food.builder()
                .product(foodDto.getProduct())
                .currency(foodDto.getCurrency())
                .company(foodDto.getCompany())
                .state(Food.State.ON_SALE)
                .build();
        foodRepository.save(newFood);
        return FoodDto.from(newFood);
    }
    @Transactional
    @Override
    public FoodPage getAll(int page) {
        //JPA нам предоставляет
        PageRequest pageRequest = PageRequest.of(page, DEFAULT_PAGE_SIZE, Sort.by("id"));
        //В ответ мы получаем страницу
        Page<Food> foods = foodRepository.findAllByState(Food.State.ON_SALE, pageRequest);

        return FoodPage.builder()
                .foodDtoList(from(foods.getContent()))
                .pageSize(foods.getSize())
                .totalPages(foods.getTotalPages())
                .build();
    }
    @Transactional
    @Override
    public FoodDto getFood(Long foodId) {
        return from(getFoodOrThrow(foodId));
    }
    @Transactional
    @Override
    public FoodDto updateFood(Long newFood, FoodDto foodDto) {
        Food food = getFoodOrThrow(newFood);

        food.setProduct(foodDto.getProduct());
        food.setCurrency(foodDto.getCurrency());
        food.setCompany(foodDto.getCompany());

        foodRepository.save(food);
        return from(food);
    }
    @Transactional
    @Override
    public void deleteFood(Long foodId) {
        Food food = getFoodOrThrow(foodId);

        food.setState(Food.State.SOLD_OUT);

        foodRepository.save(food);

    }
    @Transactional
    @Override
    public void reserveFood(Long foodId) {
        Food food = getFoodOrThrow(foodId);
        food.setState(Food.State.RESERVED);
        foodRepository.save(food);


    }

    private Food getFoodOrThrow(Long foodId) {
        return foodRepository.findById(foodId).orElseThrow(FoodNotFoundException::new);
    }
}


