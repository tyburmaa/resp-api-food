package com.tybur.blog.services;

import com.tybur.blog.dto.BuyFoodDto;
import com.tybur.blog.dto.BuyerDto;

public interface BuyerService {
    BuyerDto addBuyer(BuyerDto buyerDto);

    BuyerDto buyFood(Long buyerId, BuyFoodDto buyFoodDto);
}
