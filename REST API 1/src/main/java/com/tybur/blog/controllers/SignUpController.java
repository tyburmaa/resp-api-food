package com.tybur.blog.controllers;

import com.tybur.blog.dto.FoodPage;
import com.tybur.blog.dto.SignUpDto;
import com.tybur.blog.dto.UserDto;
import com.tybur.blog.services.SignUpService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class SignUpController {

    private final SignUpService signUpService;

    @Operation(summary = "User's sign up")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Info of created user", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDto.class))
            })
    })
    @PostMapping("/users")
    public ResponseEntity<UserDto> signUp(@RequestBody @Valid SignUpDto data) {
        return ResponseEntity
                .status(201)
                .body(signUpService.signUp(data));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    //catch the exceptiom methodArgumentNotValid
    public Map<String, String> handleValidationException(MethodArgumentNotValidException ex) {
        Map<String, String> response = new HashMap<>();
        ex.getBindingResult()
                .getAllErrors()
                .forEach(error -> {
                    response.put(((FieldError) error)
                            .getField(), error
                            .getDefaultMessage());
                });

        return response;
    }
}
