package com.tybur.blog.controllers;

import com.tybur.blog.dto.BuyFoodDto;
import com.tybur.blog.dto.BuyerDto;
import com.tybur.blog.services.BuyerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/buyers")
public class BuyerController {

    private final BuyerService buyerService;

    @PostMapping
    public ResponseEntity<BuyerDto> addBuyer(@RequestBody BuyerDto buyerDto) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(buyerService.addBuyer(buyerDto));
    }

    @PostMapping("/{buyer_id}/food")
    public ResponseEntity<BuyerDto> buyFood(@PathVariable("buyer_id") Long buyerId, @RequestBody BuyFoodDto buyFoodDto) {
        return ResponseEntity
                .status(HttpStatus.ACCEPTED)
                .body(buyerService.buyFood(buyerId, buyFoodDto));
    }
}
