package com.tybur.blog.controllers;

import com.tybur.blog.dto.FoodDto;
import com.tybur.blog.dto.FoodPage;
import com.tybur.blog.services.FoodService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/food")
public class FoodController {

    private final FoodService foodService;

    @Operation(summary = "Getting food with pagination")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Page of food", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = FoodPage.class))
            })
    })

    @GetMapping
    public ResponseEntity<FoodPage> getAllFood (@Parameter(description = "page number") @RequestParam("page") int page) {
    // позволяет передавать статусы ответа
        //статус 200
        return ResponseEntity.ok(foodService.getAll(page));

    }

    @Operation(summary = "Add food")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Make a new food and its info", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = FoodDto.class))
            })
    })
    @PostMapping
    public ResponseEntity<FoodDto> addFood (@RequestBody FoodDto foodDto) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(foodService.addFood(foodDto));
    }

    @Operation(summary = "Getting food by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Info of certain food", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = FoodPage.class))
            }),
            @ApiResponse(
                    responseCode = "404", description = "Food not found")

    })

    @GetMapping("/{food-id}")
    public ResponseEntity<FoodDto> getFood (@PathVariable("food-id") Long foodId) {
        return ResponseEntity.ok(foodService.getFood(foodId));
    }

    @PutMapping("/{food-id}")
    public ResponseEntity<FoodDto> updateFood (@PathVariable("food-id") Long foodId, @RequestBody FoodDto newFoodDto) {
        return ResponseEntity
                .accepted()
                .body(foodService.updateFood(foodId, newFoodDto));
    }

    @DeleteMapping("/{food-id}")
    public ResponseEntity<?> deleteFood (@PathVariable("food-id") Long foodId) {
        foodService.deleteFood(foodId);
        return ResponseEntity
                .accepted()
                .build();
    }

    @PostMapping(value = "/{food-id}", params = "action=reserve")
    public ResponseEntity<?> reserveFood (@PathVariable("food-id") Long foodId) {
        foodService.reserveFood(foodId);
        return ResponseEntity
                .accepted()
                .build();
    }

}
