package com.tybur.blog.controllers;

import com.tybur.blog.dto.PostsTextDto;
import com.tybur.blog.services.PostsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/posts/texts")
public class PostsController {

    private final PostsService postsService;
    @GetMapping("/texts")
    public ResponseEntity<PostsTextDto> getTexts() {
        return ResponseEntity.ok(postsService.getTexts());
    }
}
