package com.tybur.blog.models;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@EqualsAndHashCode(exclude = "buyerSet")
public class Food {

    public enum State {
        SOLD_OUT, ON_SALE, RESERVED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String product;
    private String currency;
    private String company;
    @Enumerated(value = EnumType.STRING)
    private State state;
    @ManyToMany(mappedBy = "foodSet")
    private Set<Buyer> buyerSet;
}
