package com.tybur.blog.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.xml.transform.Source;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "account")
public class User {

    public enum State {
        NOT_CONFIRMED, CONFIRMED, BANNED
    }

    public enum Role {
        ADMIN, USER
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //уникальность e-mail'а
    @Column(unique = true)
    @NotBlank(message = "{email.notBlank}")
    private String email;

    @NotBlank(message = "{hashPassword.notBlank}")
    private String hashPassword;

    @Enumerated(EnumType.STRING)
    private State state;

    @Enumerated(EnumType.STRING)
    private Role role;

}
