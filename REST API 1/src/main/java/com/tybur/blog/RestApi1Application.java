package com.tybur.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import javax.validation.Validation;
import javax.validation.Validator;


@SpringBootApplication
public class RestApi1Application {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

//   @Bean
//   public Validator validator() {
//       return Validation.buildDefaultValidatorFactory().getValidator();
//   }

    public static void main(String[] args) {
        SpringApplication.run(RestApi1Application.class, args);
    }

}
