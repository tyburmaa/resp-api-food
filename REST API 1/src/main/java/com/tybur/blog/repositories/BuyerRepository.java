package com.tybur.blog.repositories;

import com.tybur.blog.models.Buyer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BuyerRepository extends JpaRepository <Buyer, Long> {
}
