package com.tybur.blog.repositories;

import com.tybur.blog.models.Food;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodRepository extends JpaRepository<Food, Long> {

    //возможность выбора продуктов по страницам с одним состоянием
    Page<Food> findAllByState (Food.State state, Pageable pageable);
}
